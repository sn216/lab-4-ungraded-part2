import java.util.Scanner;
public class Application {
    
    public static void main (String args[])
    {
        Scanner scan = new Scanner(System.in);
        int amountStudiedPos = scan.nextInt();
        int amountStudiedNeg = scan.nextInt();

        Students x = new Students("Sriraam",18);
        x.setPc("Yes");
        
        Students y = new Students("Ryan",21);
        y.setPc("No");
        
        Students z = new Students("Chris",20);
        z.setPc("Yes");

        Students constructors = new Students("Baljeet", 21);
        constructors.setPc("No");

        Students[] section3 = new Students[3];
        section3[0] = x;
        section3[1] = y;
        section3[2] = z;
        section3[1].learn(amountStudiedPos);
        section3[2].learn(amountStudiedNeg);
   
        System.out.println(constructors.getName());
        System.out.println(constructors.getAge());
        System.out.println(constructors.getPc());



    }
}
