public class Students
{
    private String studentName;
    private String studentsWithPc;
    private int studentsAge;
    public int amountLearnt;

    public Students(String studentName, int studentsAge)
    {
        this.studentName = studentName;
        this.studentsWithPc = "Yes";
        this.studentsAge = studentsAge;
        this.amountLearnt = 0;
    }

    public String sayGreetings(String studentName)
    {
        return "Hello " + studentName;
    }

    public void  sayBringPc (String studentsWithPc)
    {
        if (studentsWithPc.equals("yes"))
        {
            System.out.println("good");
        }

        else{
            System.out.println("bring pc");
        }
    }

    public void learn (int amountStudied)
    {
        if(amountStudied > 0)
        {
            amountLearnt = amountLearnt + amountStudied;
        }

        else
        {
            amountLearnt = 0;
        }
    }

    public String getName() 
    {
        return this.studentName;
    }

    public int getAge() 
    {
        return this.studentsAge;
    }

    public String getPc() 
    {
        return this.studentsWithPc;
    }

    public void setPc(String newPc) 
    {
        this.studentsWithPc = newPc;
    }
     
 
    
}